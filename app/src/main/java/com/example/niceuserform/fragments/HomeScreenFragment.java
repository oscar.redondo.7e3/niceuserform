package com.example.niceuserform.fragments;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.navigation.NavDirections;
import androidx.navigation.Navigation;

import com.example.niceuserform.R;

public class HomeScreenFragment extends Fragment {


    Button blogin;
    Button bregister;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }


    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {


        View v = inflater.inflate(R.layout.home_fragment,container,false);

        blogin = v.findViewById(R.id.button_loggin_home);
        bregister = v.findViewById(R.id.button_register_home);


        blogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                NavDirections navDirections = HomeScreenFragmentDirections.actionHomeScreenFragmentToLoginScreenFragment();

                Navigation.findNavController(v).navigate(navDirections);
            }
        });

        bregister.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                NavDirections navDirections2 = HomeScreenFragmentDirections.actionHomeScreenFragmentToRegisterScreenActivity();
                Navigation.findNavController(v).navigate(navDirections2);
            }
        });

        return v;
    }
}
