package com.example.niceuserform.fragments;

import android.content.DialogInterface;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.fragment.app.Fragment;
import androidx.navigation.NavDirections;
import androidx.navigation.Navigation;

import com.example.niceuserform.R;
import com.example.niceuserform.activity.MainActivity;
import com.google.android.material.dialog.MaterialAlertDialogBuilder;
import com.google.android.material.textfield.TextInputEditText;
import com.google.android.material.textfield.TextInputLayout;

public class LoginScreenFragment extends Fragment {

   TextInputLayout textInputLayoutUsername,textInputLayoutPassword;
   TextInputEditText textInputEditTextUsername,textInputEditTextPassword;
   Button  bLoginLogin,bregisterLogin,bforgotpassword;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable final ViewGroup container, @Nullable Bundle savedInstanceState) {


        View v = inflater.inflate(R.layout.login_fragment,container,false);

        textInputLayoutUsername =(TextInputLayout) v.findViewById(R.id.textField_username_loggin);
        textInputEditTextUsername = v.findViewById(R.id.textInput_username_login);
        textInputLayoutPassword=v.findViewById(R.id.textField_password_login);
        textInputEditTextPassword = v.findViewById(R.id.text_input_password_loggin);
        bLoginLogin = v.findViewById(R.id.button_loggin_login);
        bregisterLogin = v.findViewById(R.id.button_register_login);
        bforgotpassword = v.findViewById(R.id.textButton_forggot_login);
        bLoginLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (!valedateInput(textInputEditTextUsername,textInputLayoutUsername,false));
                else if (!valedateInput(textInputEditTextPassword,textInputLayoutPassword,true));
                else {
                    NavDirections navDirections = LoginScreenFragmentDirections.actionLoginScreenFragmentToWelcomeScreenFragment(textInputEditTextUsername.getText().toString());

                    Navigation.findNavController(v).navigate(navDirections);
                }
            }
        });





        bregisterLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                NavDirections navDirections = LoginScreenFragmentDirections.actionLoginScreenFragmentToRegisterScreenActivity();
                Navigation.findNavController(v).navigate(navDirections);
            }
        });
        //todo preguntar context del fragments y lo d el app theme como funciona
        //todo ahora no peta pero no se muestra el dialogo
        bforgotpassword.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                MaterialAlertDialogBuilder mr=new MaterialAlertDialogBuilder(getActivity());
                mr.setTitle("Forgot password");
                mr.setMessage("To reset the  password send a  e-mail to resetpassword@niceUSerForm.com");
                mr.setPositiveButton("Ok",null);
                mr.show();


            }
        });

        return v;

    }



    public static boolean valedateInput(TextInputEditText textInputEditText,TextInputLayout  textInputLayout,boolean isPassword){

        if (textInputEditText.getText().toString().trim().isEmpty()){
            textInputLayout.setError("Can't be empty");
            return false;
        }else {
            if (isPassword){
                if (textInputEditText.getText().toString().trim().length()<8){
                    textInputLayout.setError("Must  have at least 8 caracters");
                    return false;
                }else {

                }
            }
        }
        textInputLayout.setErrorEnabled(false);
        return true;
    }
}
