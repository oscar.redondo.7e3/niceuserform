package com.example.niceuserform.fragments;

import android.app.DatePickerDialog;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.DatePicker;
import android.widget.Spinner;

import com.example.niceuserform.activity.MainActivity;
import com.google.android.material.textfield.TextInputEditText;
import com.google.android.material.textfield.TextInputLayout;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.navigation.NavDirections;
import androidx.navigation.Navigation;

import com.example.niceuserform.R;

import java.util.Calendar;

public class RegisterScreenActivity extends Fragment {

    private static TextInputEditText textInputEditTextPassword,textInputEditTextPasswordRepeat;
    private static TextInputLayout textInputLayoutRepeatPassword;
    Button bregister,blogin;
    Spinner  spinner;
    Calendar c;
    DatePickerDialog datePicker;
    TextInputLayout textInputLayoutUSername,textInputLayoutPassword,textInputLayoutName,textInputLayoutSurname,textInputLayoutemail,textInputLayoutdate;
     TextInputEditText textInputEditTextUsername,textInputEditTextname,textInputEditTextSurname,textInputEditTextEmail,textInputEditTextdate;
    CheckBox checkBox;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }


    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {




        View v = inflater.inflate(R.layout.register_fragment,container,false);
        blogin = v.findViewById(R.id.button_loggin_register);
        bregister  = v.findViewById(R.id.button_register_register);
        textInputLayoutemail = v.findViewById(R.id.textField_email_register);
        textInputLayoutUSername = v.findViewById(R.id.textField_username_register);
        textInputLayoutPassword = v.findViewById(R.id.textField_password_register);
        textInputLayoutRepeatPassword =v.findViewById(R.id.textField_repeatPassword_register);
        textInputLayoutName = v.findViewById(R.id.textField_name_register);
        textInputLayoutSurname  =v.findViewById(R.id.textField_surname_register);
        textInputLayoutdate  = v.findViewById(R.id.textField_date_register);

        checkBox = v.findViewById(R.id.checkboxConditions2);
        spinner = v.findViewById(R.id.spinner);


        textInputEditTextUsername  = v.findViewById(R.id.textInput_username_register);
        textInputEditTextPasswordRepeat  = v.findViewById(R.id.textInput_repetPAssword_register);
        textInputEditTextPassword = v.findViewById(R.id.text_input_password_register);
        textInputEditTextname = v.findViewById(R.id.textInput_name_register);
        textInputEditTextSurname =  v.findViewById(R.id.textInput_surname_register);
        textInputEditTextEmail = v.findViewById(R.id.textInput_email_register);
        textInputEditTextdate = v.findViewById(R.id.textInput_date_register);

        textInputEditTextdate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                c=Calendar.getInstance();
                int  day = c.get(Calendar.DAY_OF_MONTH);
                int  mouth =  c.get(Calendar.MONTH);
                final int year = c.get(Calendar.YEAR);
                datePicker  =  new DatePickerDialog(getContext(), new DatePickerDialog.OnDateSetListener(){

                    @Override
                    public void onDateSet(DatePicker view, int myear, int mmonth, int dayOfMonth) {
                        textInputEditTextdate.setText(dayOfMonth+"/"+(mmonth+1)+"/"+myear);
                    }
                },day,mouth,year);
            datePicker.show();
            }
        });

        blogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                NavDirections navDirections  = RegisterScreenActivityDirections.actionRegisterScreenActivityToLoginScreenFragment();
                Navigation.findNavController(v).navigate(navDirections);

            }
        });

        bregister.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (!valedateInput(textInputEditTextUsername,textInputLayoutUSername,false));
                else if (!valedateInput(textInputEditTextPassword,textInputLayoutPassword,true));
                else if (!valedateInput(textInputEditTextEmail,textInputLayoutemail,false));
                else if (!valedateInput(textInputEditTextname,textInputLayoutName,false));
                else if (!valedateInput(textInputEditTextSurname,textInputLayoutSurname,false));
                else if (!valedateInput(textInputEditTextdate,textInputLayoutdate,false));
                else if (!checkBox.isChecked()){
                    checkBox.setError("Must acept it");
                }
                else {
                    NavDirections navDirections  = RegisterScreenActivityDirections.actionRegisterScreenActivityToWelcomeScreenFragment(textInputEditTextUsername.getText().toString());
                    Navigation.findNavController(v).navigate(navDirections);
                }


            }
        });

        return v;


    }
    public static boolean valedateInput(TextInputEditText textInputEditText, TextInputLayout textInputLayout, boolean isPassword){

        if (textInputEditText.getText().toString().trim().isEmpty()){
            textInputLayout.setError("Can't be empty");
            return false;
        }else {
            if (isPassword){
                if (textInputEditText.getText().toString().trim().length()<8){
                    textInputLayout.setError("Must  have at least 8 caracters");
                    return false;
                }else {
                    if (textInputEditTextPassword.getText().toString().equals(textInputEditTextPasswordRepeat.toString())){
                        textInputLayoutRepeatPassword.setError("Password doesn't match");
                        return false;
                    }
                }
            }
        }
        textInputLayout.setErrorEnabled(false);
        return true;
    }

}
